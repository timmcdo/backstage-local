import { CatalogClient } from "@backstage/catalog-client";
import {
  createBuiltinActions,
  createRouter,
} from "@backstage/plugin-scaffolder-backend";
import { Router } from "express";
import type { PluginEnvironment } from "../types";
import { ScmIntegrations } from "@backstage/integration";

// Import skeActions
import { skeActions } from "@syntasso/plugin-ske-backend";

export default async function createPlugin(
  env: PluginEnvironment,
): Promise<Router> {
  const catalogClient = new CatalogClient({
    discoveryApi: env.discovery,
  });

  /* To preserve the builtin actions, do the following */
  const integrations = ScmIntegrations.fromConfig(env.config);
  const builtInActions = createBuiltinActions({
    catalogClient,
    integrations,
    config: env.config,
    reader: env.reader,
  });

  const actions = [
    ...skeActions({ integrations, config: env.config }),
    ...builtInActions,
  ];

  return await createRouter({
    logger: env.logger,
    config: env.config,
    database: env.database,
    reader: env.reader,
    catalogClient,
    identity: env.identity,
    permissions: env.permissions,
    actions, // <-- add this line
  });
}
